#!/usr/bin/python3

import requests   # make http request 
import hashlib    # create our md5 hash to pass to dev.marvel.com
import time       # create time stamps (for our RAND)
from pathlib import Path   # find directory path 

## Define URL
API = 'http://gateway.marvel.com/v1/public/characters'

## Calculate a hash to pass through to our MARVEL API call
## Marvel API wants md5 calc md5(ts+privateKey+publicKey)
def hashbuilder(rand, privkey, pubkey):
    return hashlib.md5((f"{rand}{privkey}{pubkey}").encode('utf-8')).hexdigest()  # create an MD5 hash of our identifers

# append timestamp, apikey and hash params 
def getuserkey():
    p = Path(__file__).parents[1]
    ## harvest private key
    privfile = str(p) + "/marvel.priv"
    with open(privfile) as pkey:
        privkey = pkey.read().rstrip('\n')
    ## harvest public key
    pubfile = str(p) + "/marvel.pub"
    with open(pubfile) as pkey:
        pubkey = pkey.read().rstrip('\n')
    ## create an integer from a float timestamp (for our RAND)
    rand = str(time.time()).rstrip('.')
    keyhash = hashbuilder(rand, privkey, pubkey)
    return f"&ts={rand}&apikey={pubkey}&hash={keyhash}";


def getCharacterByName(character):
    userkey = getuserkey()
    URL = f"{API}?name={character}{userkey}"
    ## initialize data 
    comics = []
    name = ""
    image = ""
    ## Call the webservice
    req = requests.get(URL)
    ## Convert returned data to json if get request is success
    if req.status_code == 200:
        response = req.json()
        ## Map comics, name, image with returned data if data count is more than 1 
        if response.get('data').get('count') > 0:
            ## In this case, we only retreive the first element of the results 
            comics = response.get('data').get('results')[0].get('comics').get('items')
            name = response.get('data').get('results')[0].get('name')
            image = response.get('data').get('results')[0].get('thumbnail').get('path')
            image += "." + response.get('data').get('results')[0].get('thumbnail').get('extension')
        ## Set name to Character Not Fond if data count is 0 
        else:
            name = "Character Not Found."
    ## Return mapped comics, name, image
    return comics, name, image


def getAllCharacters():
    userkey = getuserkey()
    URL = f"{API}?limit=100{userkey}"
    print(URL)
    ## initialize data
    characters = []
    ## Call the webservice
    req = requests.get(URL)
    ## Convert returned data to json if get request is success
    if req.status_code == 200:
        response = req.json()
        ## Map comics, name, image with returned data if data count is more than 1
        if response.get('data').get('count') > 0:
            response = response.get('data').get('results')
            for r in response:
                characters.append(r.get('name'))
        ## Set name to Character Not Fond if data count is 0
        else:
            characters = "No Character Found."
    return characters


