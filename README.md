# mycode (Certificate Project)

One Paragraph of your project description goes here. Describe what you're trying to do.
What is the purpose of putting up this repo?

## Getting Started

1. git clone https://gitlab.com/jinnyvifc/alta3research-mycode-cert.git
2. Make your Marvel dev account. Head on over to https://developer.marvel.com/ (You can skip this step if you already owned a Marvel dev account.)
3. Store the public key as file name "marvel.pub" into the same level as the repo which is alta3research-mycode-cert **(Please make sure it does not contains any space or new line before or after the key)**
4. Store the private key as file name "marvel.priv" into the same level as the repo which is alta3research-mycode-cert **(Please make sure it does not contains any space or new line before or after the key)**
5. Run the app 
    - cd to the repo : _cd alta3research-mycode-cert/_
    - _python3 alta3research-flask01.py_
    - go to aux1


## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Jinny** - *Initial work* - [YourWebsite](https://example.com/)
