#!/usr/bin/python3

import requests


## Define URL
URL = 'http://api.open-notify.org/astros.json'


def main():
    """runtime code"""

    ## Call the webservice
    groundctrl = requests.get(URL)

    ## strip the json off the 200 that was returned by our API
    ## translate the json into python lists and dictionaries
    helmetson = groundctrl.json()

    ## display our Pythonic data
    print("\n\nConverted Python data")
    print(helmetson)

    print('\n\nPeople in Space: ', helmetson['number'])
    people = helmetson['people']
    print(people)

if __name__ == "__main__":
    main()


