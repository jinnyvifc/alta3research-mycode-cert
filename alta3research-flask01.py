#!/usr/bin/python3
from flask import Flask # An object of Flask class is our WSGI application
from flask import render_template
from flask import request
from alta3research_requests02 import * # Get function from file alta3research_requests02 to make API request

# Flask constructor takes the name of current
# module (__name__) as argument
app = Flask(__name__)

# root route that will let user to search character 
# will return character thumbnail and comic list 
@app.route("/", methods= ["GET","POST"])
def searchCharacter():
    ## initialize variable that is used in home.html
    comics =[]
    name = ""
    image = ""
    ## when user search a character name
    ## get the input value 
    if request.method == "POST":
        character = request.form.get("character")
        ## call API to get the character if the input is not null
        if character:
            comics, name, image = getCharacterByName(character)
    ## look for templates/homt.html
    return render_template("home.html", comics=comics, name=name, image=image)

# display first 100 characters
@app.route("/characters", methods= ["GET","POST"])
def displayAllCharacters():
    characters = getAllCharacters()
    ## look for templates/homt.html
    return render_template("characters.html", characters=characters)

if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application

